from django.urls import include, path
from rest_framework import routers, permissions
from restapi import views


urlpatterns = [
    path('products', views.product_list),
]