from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from rest_framework import viewsets, status
from productmng.models import Product, ProductAttribute
from restapi.services import get_product_list, save_products


@csrf_exempt
def product_list(request):
    if request.method == 'GET':
        """
        Return a list of all products
        """
        product_list = get_product_list()
        return JsonResponse(product_list, safe=False)

    elif request.method == 'POST':
        """
        Save products to database
        """
        products = save_products(JSONParser().parse(request))

        if products is not None:
            return JsonResponse(products, status=status.HTTP_201_CREATED, safe=False)
        else:
            return JsonResponse("Could not add products", status=status.HTTP_400_BAD_REQUEST)