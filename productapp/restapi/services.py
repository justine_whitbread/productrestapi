from productmng.models import Product, ProductAttribute


def get_product_list():
    products = Product.objects.all()
    product_list = []
    """
    Query database for a list of products and attributes and structure the data to match the required json output format
    """

    for in_product in products:
        attributes = ProductAttribute.objects.filter(product_id=in_product.product_sku)
        attribute_dict = []
        # get attributes per product
        for attribute in attributes:
            attribute_dict.append({attribute.attribute_type: attribute.attribute_value})

        product_list.append({"sku": in_product.product_sku, 'attributes': attribute_dict})

    return product_list

def save_products(products):
    """
    TODO add proper validation
    """
    product_list = []
    for in_product in products:
        try:
            #save product
            product = Product(product_sku=in_product["sku"])
            product.save()
            attribute_arr = {}
            for attribute_type in in_product["attributes"]:
                #save product attributes
                attribute = ProductAttribute(attribute_type=attribute_type,
                                             attribute_value=in_product["attributes"][attribute_type], product=product)
                attribute.save()
                attribute_arr[attribute.attribute_type] = attribute.attribute_value

            #build output data
            out_product = {"sku": product.product_sku, 'created': True, 'attributes': attribute_arr}
            product_list.append(out_product)
        except:
            #skip product and continue
            out_product = {"sku": product.product_sku, 'created': False}
            product_list.append(out_product)


    return product_list
