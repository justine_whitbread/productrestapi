from django.http import HttpResponse
from django.shortcuts import render
from restapi.services import get_product_list

def index(request):
    """
    Return a list of all products
    """
    product_list = get_product_list()
    context = {'product_list': product_list}
    return render(request, 'index.html', context)
