from django.db import models

class Product(models.Model):
    product_sku = models.CharField('product sku', max_length=20, primary_key=True)
    created_date = models.DateTimeField('date created',auto_now_add=True)

class ProductAttribute(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    attribute_type = models.CharField(max_length=50)
    attribute_value = models.CharField(max_length=200)
    created_date = models.DateTimeField('date created', auto_now_add=True)
