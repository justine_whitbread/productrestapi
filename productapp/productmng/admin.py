from django.contrib import admin

# Register your models here.
from .models import Product, ProductAttribute

admin.site.register(Product)
admin.site.register(ProductAttribute)
