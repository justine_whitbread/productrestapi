from django.urls import path

from productmng import views

urlpatterns = [
    path('', views.index, name='index'),
]