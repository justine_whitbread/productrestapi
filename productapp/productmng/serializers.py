from .models import Product, ProductAttribute
from rest_framework import serializers


class ProductSerializer(serializers.Serializer):

    product_sku = serializers.CharField(required=True, allow_blank=False, max_length=20)
    created_date = serializers.DateTimeField()

    class Meta:
        model = Product
        fields = ('product_sku', 'created_date')


class ProductAttributeSerializer(serializers.Serializer):
    id = serializers.IntegerField(read_only=True)
    attribute_type = serializers.CharField(required=True, allow_blank=False, max_length=50)
    attribute_value = serializers.CharField(required=True, allow_blank=False, max_length=200)
    product = ProductSerializer(read_only=True)

    class Meta:
        model = ProductAttribute
        fields = ('attribute_type', 'attribute_value', 'product')






