# Generated by Django 3.2.5 on 2021-07-04 13:33

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Product',
            fields=[
                ('product_sku', models.CharField(max_length=20, primary_key=True, serialize=False, verbose_name='product sku')),
                ('product_description', models.CharField(max_length=200, verbose_name='product description')),
                ('created_date', models.DateTimeField(verbose_name='date created')),
            ],
        ),
        migrations.CreateModel(
            name='ProductAttribute',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('attribute_type', models.CharField(max_length=50)),
                ('attribute_value', models.CharField(max_length=200)),
                ('created_date', models.DateTimeField(verbose_name='date created')),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='productmng.product')),
            ],
        ),
    ]
