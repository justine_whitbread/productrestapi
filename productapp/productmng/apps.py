from django.apps import AppConfig


class ProductmngConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'productmng'
