##### Installation Instructions

###### Linux:

```
python3 -m venv env
source env/bin/activate
python3 -m pip install -r requirements.txt
```

###### Windows

```
python3 -m venv env
env\Scripts\activate
python3 -m pip install -r requirements.txt
```
##### About the App

The App was developed using the python django framework, django rest framework 
and bootstrap 4 and a sqlite db.

##### Using the App:

###### Rest API Endpoints

http://127.0.0.1:8000/api/products

Implements the GET and POST method as per the original specification

###### Web App Frontend

http://127.0.0.1:8000/

Displays a list of all products 

##### Admininstration Frontend

http://127.0.0.1:8000/admin/

credentials: admin/password123

The management frontend uses django's automatic admin interface to manage user access and 
create new products on the front end. 

Access to the rest API can also be controlled using this plugin, but I switched it off for the purpose of this demo.

https://docs.djangoproject.com/en/3.2/ref/contrib/admin/

##### Other Considerations
I chose to use django because it is lighter weight than symfony, and I find it to be faster and more responsive.
That being said I am not happy with the way that Django manages one to many relationships.
I had to write a second lookup per product to pull each product's individual attributes which could lead to a high overhead 
depending on the size of the product database.

Storing the product attributes as a json string directly in the products table would likely increase the speed and performance of the REST API.

It would take away the need for a second lookup on the product_attributes table and eliminate the need to reformat the data to match the json 
output format.

However, you would lose the ability to use the native django functionality to filter products using their attributes.
You would have to write additional services to filter the products using the mysql JSON search functions.





